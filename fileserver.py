import socket
import os
import sys
import subprocess
class webSocketServer:
    def __init__(self, host, port, filename):
        self.host = host
        self.port = port
        self.filename = filename
    def Start_Server(self):
        """
by austin Heisley-Cook
starts tcp fileserver that shows a list of files on remote computer server is started on.

        """
        with socket.socket(socket.AF_INET, socket.SOCK_STREAM) as s:
                    try:
                        s.bind((self.host,int(self.port)), )
                        s.listen(3)
                        while True:
                            conn, addr = s.accept()
                            data = conn.recv(1000 * 1000)
                            print(addr[0])
                            print(conn)
                            if not data:
                                print("no connection from client")
                            os.system("echo connected to client")
                            with open(self.filename, "rb") as t:
                                file = t
                                conn.sendfile(file)

                    except Exception as e:
                        print(e)


myserver = webSocketServer("", sys.argv[1], sys.argv[2])
myserver.Start_Server()
